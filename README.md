Создан инструмент для написания вариантов dasboards в графана

для подключения нового дашборда копируем json в директорию /roles/grafana/files

в json вносим изменения: изменяем порты

В коде

"datasource": {

"type": "prometheus",

"uid": "Rolan"

uid заменяем на Rolan

или может быть вариант

"datasource": "Rolan"

для получения метрик от windows_node_exporter используются аргументы,  находятся в default  роли danielweeber.windows_exporter

пример аргументов

ENABLED_COLLECTORS="ad,adfs,cache,cpu,cpu_info,cs,container,dfsr,dhcp,dns,fsrmquota,iis,logical_disk,logon,memory,msmq,mssql,netframework_clrexceptions,netframework_clrinterop,netframework_clrjit,netframework_clrloading,netframework_clrlocksandthreads,netframework_clrmemory,netframework_clrremoting,netframework_clrsecurity,net,os,process,remote_fx,service,tcp,time,vmware" TEXTFILE_DIR="C:\custom_metrics" LISTEN_PORT="9182"

# vagrant host requirements

virtualbox
vagrant
ansible
pip
winrm

sudo pip install "pywinrm>=0.2.2"

# установить плагины для vagrant

vagrant plugin install winrm
vagrant plugin install winrm-fs
vagrant plugin install winrm-elevated

# роли ansible-galaxy

ansible-galaxy role install danielweeber.windows_exporter

ansible-galaxy role install robertdebock.docker_compose

# Проверка работы

отдача метрик windows_node_exporter

http://192.168.56.100:9182/metrics

Prometheus проверка конфигурации приема метрик от windows_node_exporter

http://192.168.56.101:9090/targets?search=

Grafana адрес дашборда

http://192.168.56.101:3000/

login: admin

pass: admin
