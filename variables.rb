# windows_server
$windows_cpu = 1
$windows_memory = 1024
$windows_ip_address = "192.168.56.100"
$windows_box_name = "marcinbojko/w2k16-trial"
$windows_hostname = "wintest"
$upgrade_powershell_script = "Upgrade-PowerShell.ps1"
$config_remoting_script = "ConfigureRemotingForAnsible.ps1"
$install_python_script = "Install-Python.ps1"

#ubuntu_server
$ubuntu_cpu = 2
$ubuntu_memory = 2048
$ubuntu_ip_address = "192.168.56.101"
$ubuntu_box_name = "generic/ubuntu2004"
$ubuntu_hostname = "ubuntu-server"