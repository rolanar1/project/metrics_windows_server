Vagrant.configure("2") do |config|
  # Задание бокса
  config.vm.box = "StefanScherer/windows_2019"

  # Установка имени хоста
  config.vm.hostname = "winTest"

  # Конфигурация VirtualBox
  config.vm.provider "virtualbox" do |vb|
    vb.name = "winTest"
    vb.memory = 4096
    vb.cpus = 4
  end

  # Настройки для Windows и WinRM
  config.vm.communicator = "winrm"
  config.winrm.username = "vagrant"
  config.winrm.password = "vagrant"
  config.winrm.timeout = 1800

  # Прокидывание портов для доступа к Grafana и Prometheus
  config.vm.network "forwarded_port", guest: 3000, host: 3000 # Grafana
  config.vm.network "forwarded_port", guest: 9090, host: 9090 # Prometheus

#   # Провижонинг для установки .NET Framework 4.8 и перезагрузки
#   config.vm.provision "shell", privileged: true, inline: <<-SHELL
#     # Установка .NET Framework 4.8
#     $url = "https://download.visualstudio.microsoft.com/download/pr/2d6bb6b2-226a-4baa-bdec-798822606ff1/8494001c276a4b96804cde7829c04d7f/ndp48-x86-x64-allos-enu.exe"
#     $output = "C:\\tmp\\ndp48-x86-x64-allos-enu.exe"
#     Invoke-WebRequest -Uri $url -OutFile $output
#     Start-Process -FilePath $output -ArgumentList "/q" -Wait
#     Remove-Item -Path $output

#     # Перезагрузка системы
#     Restart-Computer -Force
#   SHELL

  # Провижонинг для установки Chocolatey
  config.vm.provision "shell", privileged: true, inline: <<-SHELL, reboot: true
    # # Ожидание после перезагрузки
    # Start-Sleep -Seconds 120

    # Установка Chocolatey
    Set-ExecutionPolicy Bypass -Scope Process -Force
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
  SHELL


end
