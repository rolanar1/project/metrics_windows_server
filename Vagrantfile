require_relative 'variables'

Vagrant.configure("2") do |config|
  # Настройка Windows Server
  config.vm.define "windows_server" do |windows|
    windows.vm.box = $windows_box_name
    windows.vm.hostname = $windows_hostname
    windows.vm.network "private_network", ip: $windows_ip_address

    windows.vm.provider "virtualbox" do |vb|
      vb.memory = $windows_memory
      vb.cpus = $windows_cpu
    end

    windows.vm.provision "shell", path: "./tools/#{$upgrade_powershell_script}"
    windows.vm.provision "shell", path: "./tools/#{$config_remoting_script}"
    windows.vm.provision "shell", path: "./tools/#{$install_python_script}"

    # Provision with Ansible on the Windows server
    windows.vm.provision "ansible" do |ansible|
      ansible.playbook = "windows_playbook.yml"
      ansible.limit = "all"
      ansible.inventory_path = "./inventory/hosts"
    end
  end

  # Настройка Ubuntu 20.04
  config.vm.define "ubuntu_server" do |ubuntu|
    ubuntu.vm.box = $ubuntu_box_name
    ubuntu.vm.hostname = $ubuntu_hostname
    ubuntu.vm.network "private_network", ip: $ubuntu_ip_address

    ubuntu.vm.provider "virtualbox" do |vb|
      vb.memory = $ubuntu_memory
      vb.cpus = $ubuntu_cpu
    end

    # Provision with Ansible on the Ubuntu server
    ubuntu.vm.provision "ansible" do |ansible|
      ansible.playbook = "ubuntu_playbook.yml"
      ansible.limit = "all"
      ansible.inventory_path = "./inventory/hosts"
    end
  end
end
